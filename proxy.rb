# my_rack_app.rb
 
require 'rack'

app = Proc.new do |env|
	req = Rack::Request.new(env)
	pretty_print(env)
	if req.post?
		pretty_print(req.params)
	end
    	['200', {'Content-Type' => 'text/html'}, ['A barebones rack app.']]
end

def pretty_print(hash)
	puts hash.map {|key,value| "#{key} => #{value}"}.sort.join("\n")
end

Rack::Handler::WEBrick.run app
